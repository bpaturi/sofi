#!/usr/bin/env bash

build_application()
{
    mvn package
}

run_application()
{
    if test -d target
    then
        if test -f target/orders*SNAPSHOT.jar
        then
            nohup java -jar target/orders-1.0-SNAPSHOT.jar server config.yml &
        else
            echo "!!! Error Occurred - JAR file required to start the application doesn't exist !!!"
        fi
    else
        echo "!!! No target directory !!!"
    fi
}

build_application
run_application