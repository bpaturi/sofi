# SoFi

**Application**

1. Run **./start.sh** to package the application and run it.
2. Run **./run.sh** to load the transactions from data.csv file and prompt the user to provide a userId to get the most frequently visited merchants.
3. To access API from a browser or postman use **http://localhost:8080/orders/mostVisitedMerchants/<user-id>**
for getting the most frequently visited merchants for a user and **http://localhost:8080/orders/add** to POST a new order.
4. For application **metrics** and **healthchecks** please use **http://localhost:8081**
---

## Implementation Assumptions and Callouts

1. Used Dropwizard to create REST APIs
2. Created basic healthchecks for the application and the main component (IOrderService).
3. Used RentrantReadWriteLock while reading and writing transactions to the inmemory transaction store.
Made this decision to improve performance assuming there will be more reads than writes. Going with a concurrent data structure
or a synchronized block might degrade the performance with mutual exclusive locking.
4. Main datastructure used for storing transactions/orders is a Map (For constant time lookups and insertions).
There is a high level Map with key being userId and value being a ObjectStore. ObjectStore is the datastructure that will hold
the internal logic to keep track of the most frequently visited merchants and the mapping between merchants and the orders.
ObjectStore internally has a Map with key being merchant and value being the list of orders associated with the merchant.
There is a separate List in the ObjectStore that will hold the most freqeuntly visited merchants. The max size of this list will be
same as the top N most visited merchants.
5. After getting clarification on the sample data converted Merchant to an enum. This will give us a way to validate the data coming in
and also the space needed to store them in the transaction data store.
6. Have general validation in place to verify the data being sent in the POST is valid.
7. Number of most visited merchants and the minimum transaction limit per user are both configurable.
8. Total time spent is about 4-5 hours. Spent good chunk of time coming with the data structures for transaction store.

**Assumptions**

1. Build scripts and the script to load data are pretty basic.
Made assumptions about the system on which the scripts will be running already has Maven and Java.


**Improvements**

1. Could have used Guice for DI.
2. Can definitely improve the scripts.
3. Write more tests
4. Include more metrics and improve the healthchecks.
5. Could have used Lombok or Immutables for POJOs
---

There is a lot I want to call out here, but this is what I am able to recall at the moment :)