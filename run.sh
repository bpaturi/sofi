#!/usr/bin/env bash

load_data()
{
    while IFS=, read -r userId merchant id price purchaseDate txId
    do
        curl "http://localhost:8080/orders/add" \
        -H "Accept: application/json" \
        -H "Content-Type:application/json" \
        --data '{ "user-id": "'"$userId"'", "merchant": "'"$merchant"'", "price": "'"$price"'", "purchase-date": "'"$purchaseDate"'", "tx-id": '"$txId"' }'
        echo "Inserted : $userId|$merchant|$id|$price|$purchaseDate|$txId"
    done < data.csv
    echo " "
}

get_most_visisted_merchants()
{
    while true
    do
        echo -e "\n\n"
        read -p "Please enter the user-id for which you want to check the most frequently visited merchants : " answer
        echo -e "\n"

            curl "http://localhost:8080/orders/mostVisitedMerchants/$answer" \
            -H "Accept: application/json" \
            -H "Content-Type:application/json" \
            --request GET
    done
}

load_data
get_most_visisted_merchants