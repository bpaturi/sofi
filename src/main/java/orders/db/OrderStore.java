package orders.db;

import orders.api.Merchant;
import orders.api.Order;

import java.util.*;

public class OrderStore implements IOrderStore {

    private List<Set<String>> mostVisitedMerchantTracker;
    private int size;
    private int mostVisitedMerchantsLimit;
    private Map<Integer, List<Order>> orders;

    public OrderStore(int mostVisitedMerchantsLimit) {
        orders = new HashMap<>();
        mostVisitedMerchantTracker = new ArrayList<>(mostVisitedMerchantsLimit);
        this.mostVisitedMerchantsLimit = mostVisitedMerchantsLimit;
    }

    @Override
    public void add(Order order) {
        Merchant merchant = order.getMerchant();
        int merchantId = merchant.id();
        List<Order> merchantOrders = orders.getOrDefault(merchantId, new ArrayList<>());
        merchantOrders.add(order);
        int sortIndex = merchantOrders.size() >= mostVisitedMerchantsLimit ? mostVisitedMerchantsLimit - 1 : merchantOrders.size() - 1;
        Set<String> merchants = mostVisitedMerchantTracker.size() == sortIndex ? null : mostVisitedMerchantTracker.get(sortIndex);
        if(merchants == null) {
            merchants = new HashSet<>();
            mostVisitedMerchantTracker.add(sortIndex, merchants);
        }
        merchants.add(merchant.value());
        if(sortIndex > 0) {
            mostVisitedMerchantTracker.get(sortIndex - 1).remove(merchant.value());
        }
        orders.put(merchantId, merchantOrders);
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public List<String> mostVisitedMerchants() {
        List<String> mostVisitedMerchants = new ArrayList<>();
        int counter = 0;
        for(int index = mostVisitedMerchantTracker.size() - 1; index >= 0; index--) {
            Set<String> merchants = mostVisitedMerchantTracker.get(index);
            for(String merchant : merchants) {
                mostVisitedMerchants.add(merchant);
                counter++;
                if(counter == mostVisitedMerchantsLimit) {
                    return mostVisitedMerchants;
                }
            }
        }

        return mostVisitedMerchants;
    }
}
