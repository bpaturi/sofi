package orders.db;

import orders.api.Order;

import java.util.List;

public interface IOrderStore {
    /**
     * Saves the given order.
     *
     * @param order
     */
    void add(Order order);

    /**
     * Calculates and returns the size which is the number of orders in constant time
     *
     * @return
     */
    int size();

    /**
     * Computes the most visited merchants based on the orders and the limit.
     *
     * @return
     */
    List<String> mostVisitedMerchants();
}
