package orders.core.service;

import orders.api.Order;
import orders.core.exception.InsufficientTransactionsException;
import orders.core.exception.UserNotFoundException;
import orders.db.IOrderStore;
import orders.db.OrderStore;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class OrderService implements IOrderService {

    private final Map<Integer, IOrderStore> orders = new HashMap<>();
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    private int mostVisitedMerchantsLimit;
    private int minTransactionLimit;

    public OrderService(int mostVisitedMerchantsLimit, int minTransactionLimit) {
        this.mostVisitedMerchantsLimit = mostVisitedMerchantsLimit;
        this.minTransactionLimit = minTransactionLimit;
    }

    @Override
    public void saveOrder(Order order) {
        int userId = order.getUserId();

        writeLock.lock();
        try {
            IOrderStore orderStore = orders.getOrDefault(userId, new OrderStore(mostVisitedMerchantsLimit));
            orderStore.add(order);
            orders.put(userId, orderStore);
        }
        finally {
            writeLock.unlock();
        }
    }

    @Override
    public List<String> mostVisitedMerchants(int userId)
            throws UserNotFoundException, InsufficientTransactionsException {
        readLock.lock();
        try {
            if(!orders.containsKey(userId)) {
                throw new UserNotFoundException();
            }
            IOrderStore orderStore = orders.get(userId);
            if (orderStore.size() < minTransactionLimit) {
                throw new InsufficientTransactionsException();
            }

            return orderStore.mostVisitedMerchants();
        }
        finally {
            readLock.unlock();
        }
    }

    @Override
    public int orderCount() {
        readLock.lock();
        try {
            int size = 0;
            for(IOrderStore orderStore : orders.values()) {
                size += orderStore.size();
            }

            return size;
        }
        finally {
            readLock.unlock();
        }
    }
}
