package orders.core.service;



import orders.api.Order;
import orders.core.exception.InsufficientTransactionsException;
import orders.core.exception.UserNotFoundException;

import java.util.List;

public interface IOrderService {
    /**
     * Saves the given order.
     *
     * @param order
     */
    void saveOrder(Order order);

    /**
     * Identifies and returns the most visited merchants for a given user.
     *
     * @param userId
     * @return
     * @throws UserNotFoundException when there are no transactions by the given user
     * @throws InsufficientTransactionsException when the number of transactions by the given user is less than the minimum limit
     */
    List<String> mostVisitedMerchants(int userId) throws UserNotFoundException, InsufficientTransactionsException;

    /**
     * Computes and returns the total number of transactions.
     *
     * @return
     */
    int orderCount();
}
