package orders.core.exception;

/**
 *  Exception thrown when the number of transactions are less than the threshold
 */
public class InsufficientTransactionsException extends Exception {
}
