package orders.core.exception;

/**
 *  Exception thrown when the user is not found.
 */
public class UserNotFoundException extends Exception {
}
