package orders.resources;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.annotation.Timed;
import orders.api.Order;
import orders.core.exception.InsufficientTransactionsException;
import orders.core.exception.UserNotFoundException;
import orders.core.service.IOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

@Path("orders")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OrderResource {
    private static final Logger log = LoggerFactory.getLogger(OrderResource.class);

    private final IOrderService orderService;
    private final MetricRegistry metricRegistry;
    private final Meter addOrderRequests;
    private final Meter mostVisitedMerchantRequests;
    private final Meter userNotFoundError;
    private final Meter insufficientTransactionsError;


    public OrderResource(IOrderService orderService, MetricRegistry metricRegistry) {
        this.orderService = orderService;
        this.metricRegistry = metricRegistry;
        this.addOrderRequests = metricRegistry.meter("addOrder");
        this.mostVisitedMerchantRequests = metricRegistry.meter("mostViewedMerchants");
        this.userNotFoundError = metricRegistry.meter("userNotFoundError");
        this.insufficientTransactionsError = metricRegistry.meter("insufficientTransactionsError");
    }

    /**
     * Saves the order to the in-memory order store.
     *
     * @param order
     * @return
     */
    @POST
    @Path("/add")
    @Timed
    public Response addOrder(Order order) {
        addOrderRequests.mark();
        this.orderService.saveOrder(order);

        return Response.ok().build();
    }

    /**
     * Finds the top N most visited merchants for a given user.
     *
     * Returns response with top N most visited merchant names if the user exists and the number of
     * transactions by the user is greater than the threshold.
     *
     * Returns an error if the user doesn't exist
     *
     * Returns an error if the number of transactions by the user is less than the threshold.
     *
     * @param userId
     * @return {@link Response}
     *
     */
    @GET
    @Path("/mostVisitedMerchants/{userId}")
    @Timed
    public Response mostFrequentlyVisitedMerchants(@PathParam("userId") int userId) {
        try {
            mostVisitedMerchantRequests.mark();
            List<String> mostVisitedMerchants = this.orderService.mostVisitedMerchants(userId);

            return Response.ok().entity(mostVisitedMerchants).build();
        }
        catch (UserNotFoundException unfe) {
            userNotFoundError.mark();
            log.error("User with user id {} does not exist in the store.", userId);
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new HashMap<String, String>() {{
                    put("Error", "User Does Not Exist");
                }}).build();
        }
        catch (InsufficientTransactionsException ite) {
            insufficientTransactionsError.mark();
            log.info("Not enough transactions are present for user {}", userId);
            return Response.status(Response.Status.OK).entity(
                    new HashMap<String, String>() {{
                        put("Error", "Too Few Transactions");
                    }}).build();
        }

    }
}
