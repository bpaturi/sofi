package orders;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;

public class SoFiConfiguration extends Configuration {

    /**
     * Specifies the expected date format for the orders
     */
    @NotEmpty
    private String dateFormat;

    /**
     * Specifies the number of most visited merchants the application needs to keep track of for every user
     */
    private int mostVisitedMerchantsLimit;

    /**
     * Specifies the minimum number of transactions that need to exist for a user.
     */
    private int minTransactionLimit;

    @JsonProperty
    public String getDateFormat() {
        return dateFormat;
    }

    @JsonProperty
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @JsonProperty
    public int getMostVisitedMerchantsLimit() {
        return mostVisitedMerchantsLimit;
    }

    @JsonProperty
    public void setMostVisitedMerchantsLimit(int mostVisitedMerchantsLimit) {
        this.mostVisitedMerchantsLimit = mostVisitedMerchantsLimit;
    }

    @JsonProperty
    public int getMinTransactionLimit() {
        return minTransactionLimit;
    }

    @JsonProperty
    public void setMinTransactionLimit(int minTransactionLimit) {
        this.minTransactionLimit = minTransactionLimit;
    }
}
