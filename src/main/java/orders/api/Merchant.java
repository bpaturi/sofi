package orders.api;

import java.util.HashMap;
import java.util.Map;

public enum Merchant {
    BARTELL_DRUGS(1, "Bartell Drugs"),
    SAFEWAY(2, "Safeway"),
    MCDONALDS(3, "McDonalds"),
    FLYING_PIE_PIZZA(4, "Flying Pie Pizza"),
    LOLA(5, "Lola"),
    MATTS_IN_THE_MARKET(10, "Matt's in the Market"),
    ARBYS(11, "Arbys"),
    ACME_INC(101, "Acme Inc"),
    ALBERTSONS(201, "Albertsons");

    private String value;
    private int id;

    private static Map<Integer, Merchant> idMerchantMap = new HashMap<>();

    private static void populateIdMerchantCache() {
        for(Merchant merchant : Merchant.values()) {
            idMerchantMap.put(merchant.id(), merchant);
        }
    }

    Merchant(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public int id() {
        return this.id;
    }

    public static Merchant fromId(int id) {
        return idMerchantMap.get(id);
    }

    @Override
    public String toString() {
        return this.value;
    }
}
