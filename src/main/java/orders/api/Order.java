package orders.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;

public class Order {
    @NotEmpty
    private int userId;
    @NotEmpty
    private Merchant merchant;
    @NotEmpty
    private double price;
    @NotEmpty
    private Date purchaseDate;
    @NotEmpty
    private int transactionId;

    @JsonCreator
    public Order(@JsonProperty("user-id") int userId,
                 @JsonProperty("merchant") Merchant merchant,
                 @JsonProperty("price") double price,
                 @JsonProperty("purchase-date") Date purchaseDate,
                 @JsonProperty("tx-id") int transactionId) {
        this.userId = userId;
        this.merchant = merchant;
        this.price = price;
        this.purchaseDate = purchaseDate;
        this.transactionId = transactionId;
    }

    @JsonProperty("user-id")
    public int getUserId() {
        return userId;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public double getPrice() {
        return price;
    }

    @JsonProperty("purchase-date")
    public Date getPurchaseDate() {
        return purchaseDate;
    }


    @JsonProperty("tx-id")
    public int getTransactionId() {
        return transactionId;
    }
}
