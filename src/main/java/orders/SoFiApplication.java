package orders;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import orders.core.exception.WebExceptionMapper;
import orders.core.service.IOrderService;
import orders.core.service.OrderService;
import orders.health.AppHealthCheck;
import orders.health.OrderStoreHealthCheck;
import orders.resources.OrderResource;

import java.text.SimpleDateFormat;

public class SoFiApplication extends Application<SoFiConfiguration> {

    public static void main(final String[] args) throws Exception {
        new SoFiApplication().run(args);
    }

    @Override
    public String getName() {
        return "SoFi";
    }

    @Override
    public void initialize(final Bootstrap<SoFiConfiguration> bootstrap) {

    }

    @Override
    public void run(final SoFiConfiguration configuration,
                    final Environment environment) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(configuration.getDateFormat());
        environment.getObjectMapper().setDateFormat(dateFormat);
        IOrderService orderService = new OrderService(configuration.getMostVisitedMerchantsLimit(),
                configuration.getMinTransactionLimit());
        OrderResource orderResource = new OrderResource(orderService, environment.metrics());

        environment.jersey().register(orderResource);
        environment.jersey().register(new WebExceptionMapper());
        environment.healthChecks().register("Order App", new AppHealthCheck());
        environment.healthChecks().register("Order Store", new OrderStoreHealthCheck(orderService));
    }

}
