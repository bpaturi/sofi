package orders.health;

import com.codahale.metrics.health.HealthCheck;
import orders.core.service.IOrderService;

public class OrderStoreHealthCheck extends HealthCheck {
    private final IOrderService orderService;

    public OrderStoreHealthCheck(IOrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    protected Result check() throws Exception {
        if(orderService.orderCount() >= 0)
            return Result.healthy();
        return Result.unhealthy("Order Store is unhealthy");
    }
}
