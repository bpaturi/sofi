package orders.core.service;

import orders.api.Merchant;
import orders.api.Order;
import orders.core.exception.InsufficientTransactionsException;
import orders.core.exception.UserNotFoundException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class OrderServiceTest {

    private static IOrderService orderService;

    @BeforeClass
    public static  void setup() {
        orderService = new OrderService(2, 5);
        populateOrders();
    }


    @Test
    public void saveOrder_withAValidOrder_shouldSaveInTheOrderStore() {
        assertEquals(6, orderService.orderCount());
    }

    @Test
    public void mostVisitedMerchants_withAnExistingUserId_shouldReturnTheTop2MostVisitedMerchants() throws Exception {
        List<String> mostVisitedMerchants = orderService.mostVisitedMerchants(1);


        assertEquals(2, mostVisitedMerchants.size());
        assertTrue(mostVisitedMerchants.contains(Merchant.BARTELL_DRUGS.value()));
        assertTrue(mostVisitedMerchants.contains(Merchant.ALBERTSONS.value()));
    }

    @Test(expected = UserNotFoundException.class)
    public void mostVisitedMerchants_withANonExistingUser_shouldThrowsUserNotFoundException() throws Exception {
        orderService.mostVisitedMerchants(4);
    }

    @Test(expected = InsufficientTransactionsException.class)
    public void mostVisitedMerchants_withAUserNotHavingEnoughTransactions_shouldThrowInsufficientTransactionsException()
        throws Exception {
        orderService.mostVisitedMerchants(2);
    }

    private static void populateOrders() {
        Order[] orders = {
                new Order(1, Merchant.BARTELL_DRUGS, 110, new Date(), 1),
                new Order(1, Merchant.BARTELL_DRUGS, 120, new Date(), 2),
                new Order(1, Merchant.ALBERTSONS, 130, new Date(), 3),
                new Order(1, Merchant.FLYING_PIE_PIZZA, 140, new Date(), 4),
                new Order(1, Merchant.ALBERTSONS, 130, new Date(), 5),
                new Order(2, Merchant.ALBERTSONS, 130, new Date(), 6)};

        for(Order order : orders) {
            orderService.saveOrder(order);
        }
    }
}
