package orders.db;

import orders.api.Merchant;
import orders.api.Order;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OrderStoreTest {

    private IOrderStore orderStore;

    @Before
    public void setup() {
        orderStore = new OrderStore(2);
    }

    @Test
    public void add_givenAValidOrder_addsTheOrderToStore() {
        populateOrders();

        assertEquals(5, orderStore.size());
    }

    @Test
    public void size_returnsTheTotalNumberOfOrdersInTheStore() {
        populateOrders();

        assertEquals(5, orderStore.size());
    }

    @Test
    public void mostVisitedMerchants_returnsTheTop2MerchantsBasedOnTheOrderCount() {
        populateOrders();
        List<String> mostVisitedMerchants = orderStore.mostVisitedMerchants();

        assertEquals(2, mostVisitedMerchants.size());
        assertTrue(mostVisitedMerchants.contains(Merchant.BARTELL_DRUGS.value()));
        assertTrue(mostVisitedMerchants.contains(Merchant.ALBERTSONS.value()));
    }

    private void populateOrders() {
        Order[] orders = {
                new Order(1, Merchant.BARTELL_DRUGS, 110, new Date(), 1),
                new Order(1, Merchant.BARTELL_DRUGS, 120, new Date(), 2),
                new Order(1, Merchant.ALBERTSONS, 130, new Date(), 3),
                new Order(1, Merchant.FLYING_PIE_PIZZA, 140, new Date(), 4),
                new Order(1, Merchant.ALBERTSONS, 130, new Date(), 5)};

        for(Order order : orders) {
            orderStore.add(order);
        }
    }
}
